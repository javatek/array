package org.bitbucket.javatek.array;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public final class ArrayTest {
  @Test
  public void testEmptyArray() {
    Array<String> empty = new Array<>();
    assertTrue(empty.isEmpty());
    assertEquals(0, empty.size());
  }

  @Test
  public void testAdd() {
    Array<String> array = new Array<>();

    array = array.add("first");
    array = array.add("second");
    array = array.add("third");

    assertFalse(array.isEmpty());
    assertEquals(3, array.size());

    Iterator<String> iterator = array.iterator();
    assertEquals("first", iterator.next());
    assertEquals("second", iterator.next());
    assertEquals("third", iterator.next());
  }

  @Test(expected = UnsupportedOperationException.class)
  public void testIteratorDoesNotAllowRemove() {
    Array<String> array = new Array<>("first", "second", "third");
    array.iterator().remove();
  }
}