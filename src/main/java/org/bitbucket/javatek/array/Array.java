package org.bitbucket.javatek.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static java.util.Collections.emptyList;

/**
 *
 */
public final class Array<E> implements Iterable<E> {
  private final List<E> elements;

  public Array() {
    this(emptyList());
  }

  public Array(Iterable<E> iterable) {
    this(collect(iterable));
  }

  @SafeVarargs
  public Array(E... elements) {
    this(Arrays.asList(elements));
  }

  private Array(List<E> elements) {
    this.elements = elements;
  }

  private static <E> List<E> collect(Iterable<E> iterable) {
    List<E> list = new ArrayList<>();
    for (E e : iterable)
      list.add(e);
    return list;
  }

  public int size() {
    return elements.size();
  }

  public boolean isEmpty() {
    return elements.isEmpty();
  }

  public Iterator<E> iterator() {
    Iterator<E> iterator = elements.iterator();
    return new Iterator<E>() {
      @Override
      public boolean hasNext() {
        return iterator.hasNext();
      }

      @Override
      public E next() {
        return iterator.next();
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException(
          "iterator.remove() is forbidden for Array");
      }
    };
  }

  public Array<E> add(E e) {
    List<E> elements = new ArrayList<>(this.elements);
    elements.add(e);
    return new Array<>(elements);
  }

  public Array<E> addAll(Iterable<E> iterable) {
    List<E> elements = new ArrayList<>(this.elements);
    for (E e : iterable)
      elements.add(e);
    return new Array<>(elements);
  }

  @Override
  public int hashCode() {
    return elements.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return o == this ||
      (o != null
        && o.getClass() == Array.class
        && ((Array<?>) o).elements.equals(this.elements)
      );
  }

  @Override
  public String toString() {
    return elements.toString();
  }
}
